package com.company.SelDataBase5;

import com.company.SelDataBase2.JoinTableUserDG;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class JoinMapper extends Mapper<Object, Text, Text, IntWritable> {
    Map<Long, JoinTable> joinTableMap = new HashMap<>();
    @Override
    protected void setup(Context context) throws IOException {
        Configuration configuration = new Configuration();
        configuration.set("fs.defaultFS", "hdfs://0.0.0.0:9000/");
        FileSystem fileSystem = FileSystem.get(configuration);
        FSDataInputStream open = fileSystem.open(new Path("/user/sergeimakarov/courses/user"));
        BufferedReader bf = new BufferedReader(new InputStreamReader(open));
        for (String line; (line = bf.readLine()) != null; ) {
            if(!line.equals("")){
                String[] values = line.split("\\|");
                JoinTable joinTable = new JoinTable();
                joinTable.setUser_id(Long.parseLong(values[0]));
                joinTable.setFIO(values[9], values[10]);
                joinTable.setRegistration_type(values[5]);
                joinTableMap.put(joinTable.getUser_id(), joinTable);
            }
        }
        bf.close();
        open.close();
    }

    protected void map(Object key, Text value, Context context) throws IOException, InterruptedException {
        String[] row = value.toString().split("\\|");
        JoinTable joinTable;
        if(!Objects.equals(value.toString(), "")) {
            if (joinTableMap.containsKey(Long.parseLong(row[1]))) {
                joinTable = joinTableMap.get(Long.parseLong(row[1]));
                joinTable.setMessage_text(row[6]);
                context.write(new Text(String.valueOf(joinTable.getUser_id())), joinTable);
            }
        }
    }
}
