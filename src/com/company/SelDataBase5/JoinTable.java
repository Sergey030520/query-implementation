package com.company.SelDataBase5;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class JoinTable extends IntWritable implements Writable {
    private long user_id;
    private String first_name;
    private String last_name;
    private String message_text;
    private String registration_type;

    public void setRegistration_type(String registration_type) {
        this.registration_type = registration_type;
    }

    public void setMessage_text(String message_text) {
        this.message_text = message_text;
    }
    public void setFIO(String first_name, String last_name){
        this.first_name = first_name;
        this.last_name = last_name;
    }
    public void setUser_id(long user_id) {
        this.user_id = user_id;
    }

    public long getUser_id() {
        return user_id;
    }
    public String getFIO(){
        return first_name + " " + last_name;
    }
    public String getMessage_text() {
        return message_text;
    }
    public String getRegistration_type() {
        return registration_type;
    }

    public JoinTable(){}

    @Override
    public void write(DataOutput dataOutput) throws IOException {
        dataOutput.writeLong(user_id);
        dataOutput.writeUTF(message_text);
        dataOutput.writeUTF(first_name);
        dataOutput.writeUTF(last_name);
        dataOutput.writeUTF(registration_type);
    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {
        user_id = dataInput.readLong();
        message_text = dataInput.readUTF();
        first_name = dataInput.readUTF();
        last_name = dataInput.readUTF();
        registration_type = dataInput.readUTF();
    }
}
