package com.company.SelDataBase5;


import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;

public class SelDB5 {
    private Path pathUser = new Path("/user/sergeimakarov/courses/user_private_message");

    public SelDB5(Configuration configuration) throws IOException, InterruptedException, ClassNotFoundException {
        FileSystem fileSystem = FileSystem.get(configuration);
        fileSystem.delete(new Path("result_courses/sel5"), true);
        Job job = Job.getInstance(configuration, "user");
        job.setJarByClass(SelDB5.class);
        job.setMapperClass(JoinMapper.class);
        job.setReducerClass(JoinReducer.class);

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(JoinTable.class);
        FileInputFormat.addInputPath(job, pathUser);

        FileOutputFormat.setOutputPath(job, new Path("result_courses/sel5"));
        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
}
