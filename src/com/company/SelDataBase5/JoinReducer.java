package com.company.SelDataBase5;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.Objects;

public class JoinReducer  extends Reducer<Text, JoinTable,Text,Text> {
    public void reduce(Text key, Iterable<JoinTable> values, Context context)
            throws IOException, InterruptedException {
        for(JoinTable joinTable : values){
            if(Objects.equals(joinTable.getRegistration_type(), "vk")) {
                context.write(key, new Text(joinTable.getFIO() + " " + joinTable.getMessage_text()));
            }
        }
    }
}
