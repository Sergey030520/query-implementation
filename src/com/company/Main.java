package com.company;


import com.company.SelDataBase1.SelDB1;
import com.company.SelDataBase2.SelDB2;
import com.company.SelDataBase3.SelDB3;
import com.company.SelDataBase4.SelDB4;
import com.company.SelDataBase5.SelDB5;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;


import java.io.IOException;


public class Main {

    private static Path pathCourses = new Path("/user/sergeimakarov/courses/discussion_group");
    public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException {
        Configuration configuration = new Configuration();
        configuration.set("fs.defaultFS", "hdfs://0.0.0.0:9000/");
        //SelDB1 selDB1 = new SelDB1(configuration);
        //SelDB2 selDB2 = new SelDB2(configuration);
        //SelDB3 selDB3 = new SelDB3(configuration);
        //SelDB4 selDB4 = new SelDB4(configuration);
        SelDB5 selDB5 = new SelDB5(configuration);
    }
}
