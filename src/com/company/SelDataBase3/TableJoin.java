package com.company.SelDataBase3;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class TableJoin extends IntWritable implements Writable {
    private String first_name = "";
    private String last_name = "";
    private boolean approved;

    public void setFIO(String first_name, String last_name) {
        this.first_name = first_name;
        this.last_name = last_name;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }

    public boolean getApproved(){
        return approved;
    }

    public String getFIO() {
        return first_name + " " + last_name;
    }


    @Override
    public void write(DataOutput dataOutput) throws IOException {
        dataOutput.writeUTF(first_name);
        dataOutput.writeUTF(last_name);
        dataOutput.writeBoolean(approved);
    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {
        first_name = dataInput.readUTF();
        last_name = dataInput.readUTF();
        approved = dataInput.readBoolean();
    }

}
