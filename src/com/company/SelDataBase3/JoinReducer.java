package com.company.SelDataBase3;


import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.Objects;


public class JoinReducer extends org.apache.hadoop.mapreduce.Reducer<Text, TableJoin,Text,Text> {
    public void reduce(Text key,  Iterable<TableJoin> values, Context context)
            throws IOException, InterruptedException {
        for (TableJoin tableJoin : values) {
            context.write(key, new Text(" FIO: " + tableJoin.getFIO() + " approved: " + tableJoin.getApproved()));
        }
    }
}
