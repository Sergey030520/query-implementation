package com.company.SelDataBase3;


import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;

public class SelDB3 {
    private Path pathUTDG = new Path("/user/sergeimakarov/courses/user_to_discussion_groups");

    public SelDB3(Configuration configuration) throws IOException, InterruptedException, ClassNotFoundException {
        FileSystem fileSystem = FileSystem.get(configuration);
        fileSystem.delete(new Path("result_courses/sel3"), true);

        Job job = Job.getInstance(configuration, "user");
        job.setJarByClass(SelDB3.class);

        job.setMapperClass(Mapper1.class);

        job.setReducerClass(JoinReducer.class);

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(TableJoin.class);
        FileInputFormat.addInputPath(job, pathUTDG);

        FileOutputFormat.setOutputPath(job, new Path("result_courses/sel3"));
        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
}
