package com.company.SelDataBase3;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Mapper1 extends Mapper<Object, Text, Text, IntWritable> {
    Map<String, TableJoin> joinTableMap = new HashMap<>();

    @Override
    protected void setup(Context context) throws IOException {
        Configuration configuration = new Configuration();
        configuration.set("fs.defaultFS", "hdfs://0.0.0.0:9000/");
        FileSystem fileSystem = FileSystem.get(configuration);
        FSDataInputStream open = fileSystem.open(new Path("/user/sergeimakarov/courses/user"));
        BufferedReader bf = new BufferedReader(new InputStreamReader(open));
        for (String line; (line = bf.readLine()) != null; ) {
            if(!line.equals("")){
                String[] values = line.split("\\|");
                TableJoin tableJoin = new TableJoin();
                tableJoin.setFIO(values[9], values[10]);
                joinTableMap.put(values[0], tableJoin);
            }
        }
        bf.close();
        open.close();
    }

    @Override
    protected void map(Object key, Text value, Context context) throws IOException, InterruptedException {
        String[] col = value.toString().split("\\|");
        TableJoin joinTable;
        if(!Objects.equals(value.toString(), "")) {
            System.out.println("Approved: " + col[3]);
            if (joinTableMap.containsKey(col[0]) && Objects.equals(col[3], "false")) {
                TableJoin tableJoin = joinTableMap.get(col[0]);
                tableJoin.setApproved(Boolean.getBoolean(col[3]));
                context.write(new Text(String.valueOf(col[1])), tableJoin);
            }
        }
    }
}
