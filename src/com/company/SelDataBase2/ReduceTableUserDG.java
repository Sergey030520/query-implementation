package com.company.SelDataBase2;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.Objects;

public class ReduceTableUserDG  extends Reducer<Text, JoinTableUserDG,Text,Text> {
    private IntWritable result = new IntWritable();

    public void reduce(Text key, Iterable<JoinTableUserDG> values, Context context)
            throws IOException, InterruptedException {
        for(JoinTableUserDG joinTableUserDG : values){
            if(Objects.equals(joinTableUserDG.getRegistration_type(), "vk") ||
                    Objects.equals(joinTableUserDG.getRegistration_type(), "facebook")) {
                context.write(key, new Text(joinTableUserDG.getFIO() + " " + joinTableUserDG.getEmail() +
                        " " + joinTableUserDG.getRegistration_type()));
            }
        }
    }
}
