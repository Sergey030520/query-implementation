package com.company.SelDataBase2;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class JoinTableUserDG extends IntWritable implements Writable {
    private long admin_user_id;
    private long id_user;
    private String email;
    private String registration_type;
    private String first_name;
    private String last_name;

    public JoinTableUserDG() {

    }

    public void setAdmin_user_id(long admin_user_id) {
        this.admin_user_id = admin_user_id;
    }
    public void setId_user(long id) {
        this.id_user = id;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public void setRegistration_type(String registration_type) {
        this.registration_type = registration_type;
    }
    public void setFIO(String first_name, String last_name) {
        this.first_name = first_name;
        this.last_name = last_name;
    }

    public long getAdmin_user_id() {
        return admin_user_id;
    }

    public String getEmail() {
        return email;
    }

    public String getRegistration_type() {
        return registration_type;
    }

    public long getId_user() {
        return id_user;
    }

    public String getFIO() {
        return first_name + " " + last_name;
    }

    @Override
    public void write(DataOutput dataOutput) throws IOException {
        dataOutput.writeLong(id_user);
        dataOutput.writeLong(admin_user_id);
        dataOutput.writeUTF(email);
        dataOutput.writeUTF(registration_type);
        dataOutput.writeUTF(first_name);
        dataOutput.writeUTF(last_name);
    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {
        id_user = dataInput.readLong();
        admin_user_id = dataInput.readLong();
        email = dataInput.readUTF();
        registration_type = dataInput.readUTF();
        first_name = dataInput.readUTF();
        last_name = dataInput.readUTF();

    }
}