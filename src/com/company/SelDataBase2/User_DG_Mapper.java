package com.company.SelDataBase2;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class User_DG_Mapper extends Mapper<Object, Text, Text, IntWritable> {
    Map<Long, JoinTableUserDG> joinTableUserDGMap = new HashMap<>();
    @Override
    protected void setup(Context context) throws IOException {
        Configuration configuration = new Configuration();
        configuration.set("fs.defaultFS", "hdfs://0.0.0.0:9000/");
        FileSystem fileSystem = FileSystem.get(configuration);
        FSDataInputStream open = fileSystem.open(new Path("/user/sergeimakarov/courses/user"));
        BufferedReader bf = new BufferedReader(new InputStreamReader(open));
        for (String line; (line = bf.readLine()) != null; ) {
            if(!line.equals("")){
                String[] values = line.split("\\|");
                JoinTableUserDG tableUser = new JoinTableUserDG();
                tableUser.setId_user(Long.parseLong(values[0].trim()));
                tableUser.setEmail(values[4]);
                tableUser.setFIO(values[9], values[10]);
                tableUser.setRegistration_type(values[5]);
                joinTableUserDGMap.put(tableUser.getId_user(), tableUser);
            }
        }
        bf.close();
        open.close();
    }

    protected void map(Object key, Text value, Context context) throws IOException, InterruptedException {
        String[] row = value.toString().split("\\|");
        JoinTableUserDG joinTableUserDG;
        if(!Objects.equals(value.toString(), "")) {
            if (joinTableUserDGMap.containsKey(Long.parseLong(row[5]))) {
                joinTableUserDG = joinTableUserDGMap.get(Long.parseLong(row[5]));
                joinTableUserDG.setAdmin_user_id(Long.parseLong(row[5]));
                context.write(new Text(String.valueOf(joinTableUserDG.getAdmin_user_id())), joinTableUserDG);
            }
        }
    }
}