package com.company.SelDataBase2;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;

public class SelDB2 {

    private Path pathCourses = new Path("/user/sergeimakarov/courses/discussion_group");

    public SelDB2(Configuration configuration) throws IOException, InterruptedException, ClassNotFoundException {
        FileSystem fileSystem = FileSystem.get(configuration);
        fileSystem.delete(new Path("/result_courses/sel2"), true);
        Job job = Job.getInstance(configuration, "user");
        job.setJarByClass(SelDB2.class);
        job.setMapperClass(User_DG_Mapper.class);
        job.setReducerClass(ReduceTableUserDG.class);

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(JoinTableUserDG.class);
        FileInputFormat.addInputPath(job, pathCourses);

        FileOutputFormat.setOutputPath(job, new Path("result_courses/sel2"));
        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }

}
