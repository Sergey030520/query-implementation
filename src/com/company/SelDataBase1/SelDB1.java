package com.company.SelDataBase1;


import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.MultipleInputs;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;

public class SelDB1 {
    private Path pathUser = new Path("/user/sergeimakarov/courses/user_private_message");

    public SelDB1(Configuration configuration) throws IOException, InterruptedException, ClassNotFoundException {
        FileSystem fileSystem = FileSystem.get(configuration);
        fileSystem.delete(new Path("result_courses/sel1"), true);

        Job job = Job.getInstance(configuration, "user");
        job.setJarByClass(SelDB1.class);

        job.setMapperClass(JoinMapper.class);

        job.setReducerClass(JoinReduce.class);

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(JoinTableUPM_U.class);
        FileInputFormat.addInputPath(job, pathUser);

        FileOutputFormat.setOutputPath(job, new Path("result_courses/sel1"));
        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
}
