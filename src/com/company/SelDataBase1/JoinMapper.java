package com.company.SelDataBase1;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class JoinMapper extends Mapper<Object, Text, Text, IntWritable> {
    Map<String, JoinTableUPM_U> joinTableMap = new HashMap<>();
    @Override
    protected void setup(Context context) throws IOException {
        Configuration configuration = new Configuration();
        configuration.set("fs.defaultFS", "hdfs://0.0.0.0:9000/");
        FileSystem fileSystem = FileSystem.get(configuration);
        FSDataInputStream open = fileSystem.open(new Path("/user/sergeimakarov/courses/user"));
        BufferedReader bf = new BufferedReader(new InputStreamReader(open));
        for (String line; (line = bf.readLine()) != null; ) {
            if(!line.equals("")){
                String[] values = line.split("\\|");
                JoinTableUPM_U joinTableUPMU = new JoinTableUPM_U();
                joinTableUPMU.setFio_from_user(values[9] + " " + values[10]);
                joinTableMap.put(values[0], joinTableUPMU);
            }
        }
        bf.close();
        open.close();
    }
    @Override
    protected void map(Object key, Text value, Context context) throws IOException, InterruptedException {
        String[] row = value.toString().split("\\|");
        JoinTableUPM_U joinTableUPM_u;
        if(!Objects.equals(value.toString(), "")) {
            if (joinTableMap.containsKey(row[1]) && joinTableMap.containsKey(row[2])) {
               joinTableUPM_u = joinTableMap.get(row[1]);
               joinTableUPM_u.setFio_to_user(joinTableMap.get(row[2]).getFio_from_user());
               joinTableUPM_u.setMessage_text(row[6]);
                context.write(new Text(row[0]), joinTableUPM_u);
            }
        }
    }
}
