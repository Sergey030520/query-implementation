package com.company.SelDataBase1;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class JoinTableUPM_U extends IntWritable implements Writable {
    private String fio_from_user = "";
    private String fio_to_user = "";
    private String message_text = "";

    JoinTableUPM_U(){}

    public void setFio_from_user(String fio_from_user) {
       this.fio_from_user = fio_from_user;
    }
    public void setFio_to_user(String fio_to_user) {
        this.fio_to_user = fio_to_user;
    }

    public void setMessage_text(String message_text) {
        this.message_text = message_text;
    }

    public String getMessage_text() {
        return message_text;
    }

    public String getFio_from_user() {
        return fio_from_user;
    }
    public String getFio_to_user(){
        return fio_to_user;
    }

    @Override
    public void write(DataOutput dataOutput) throws IOException {
        dataOutput.writeUTF(fio_from_user);
        dataOutput.writeUTF(fio_to_user);
        dataOutput.writeUTF(message_text);
    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {
        fio_from_user = dataInput.readUTF();
        fio_to_user = dataInput.readUTF();
        message_text = dataInput.readUTF();
    }

}
