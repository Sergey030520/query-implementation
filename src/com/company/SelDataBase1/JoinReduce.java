package com.company.SelDataBase1;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;


public class JoinReduce  extends Reducer<Text, JoinTableUPM_U, Text, Text> {
    public void reduce(Text key, Iterable<JoinTableUPM_U> values, Context context)
            throws IOException, InterruptedException {
       StringBuilder result = new StringBuilder();
        for (JoinTableUPM_U joinTableUPMU : values){
            result.append(" fio sender: ").append(joinTableUPMU.getFio_from_user())
                    .append(" fio recipient: ").append(joinTableUPMU.getFio_to_user())
                    .append(" message_text: ").append(joinTableUPMU.getMessage_text());
       }
        context.write(key, new Text(result.toString()));
    }
}