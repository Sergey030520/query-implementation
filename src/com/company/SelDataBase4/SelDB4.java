package com.company.SelDataBase4;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;

public class SelDB4 {
    private Path pathUser = new Path("/user/sergeimakarov/courses/user");

    public SelDB4(Configuration configuration) throws IOException, InterruptedException, ClassNotFoundException {
        FileSystem fileSystem = FileSystem.get(configuration);
        fileSystem.delete(new Path("result_courses/sel4"), true);
        Job job = Job.getInstance(configuration, "user");
        job.setJarByClass(SelDB4.class);
        job.setMapperClass(GroupUserMapper.class);
        job.setReducerClass(GroupReduce.class);

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);
        FileInputFormat.addInputPath(job, pathUser);

        FileOutputFormat.setOutputPath(job, new Path("result_courses/sel4"));
        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
}
